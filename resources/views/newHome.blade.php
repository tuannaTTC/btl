@extends('User.layout.layout')
@section('content')
    <section class="hero-area bg-1 text-center overly">
        <!-- Container Start -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Header Contetnt -->
                    <div class="content-block">
                        <h1>THỰC TẬP Ở ĐÂU - Ở FIND JOB</h1>
                        <p><br> Website tìm việc và tuyển dụng thực tập sinh hiệu quả nhất hiện nay! Find Job</p>
                        <div class="short-popular-category-list text-center">
                            <h2>Ngôn ngữ phổ biến</h2>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href="{{route('home.showJobGet')}}"><i class="fa fa-bed"></i> Java</a></li>
                                <li class="list-inline-item">
                                    <a href="{{route('home.showJobGet')}}"><i class="fa fa-grav"></i> Python</a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="{{route('home.showJobGet')}}"><i class="fa fa-car"></i> C#</a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="{{route('home.showJobGet')}}"><i class="fa fa-cutlery"></i> Android studio</a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="{{route('home.showJobGet')}}"><i class="fa fa-coffee"></i> Ngôn ngữ khác</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- Advance Search -->
                    <div class="advance-search">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-12 col-md-12 align-content-center">
                                    <form>
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <input type="text" class="form-control my-2 my-lg-1" id="inputtext4" placeholder="Tên công việc bạn muốn ứng tuyển">
                                            </div>
                                            <div class="form-group col-md-3">
                                                <select class="w-100 form-control mt-lg-1 mt-md-2">
                                                    <option>Địa điểm</option>
                                                    <option value="1">Top rated</option>
                                                    <option value="2">Lowest Price</option>
                                                    <option value="4">Highest Price</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <select class="w-100 form-control mt-lg-1 mt-md-2">
                                                    <option>Công việc</option>
                                                    <option value="1">Top rated</option>
                                                    <option value="2">Lowest Price</option>
                                                    <option value="4">Highest Price</option>
                                                </select>
                                            </div>

                                            <div class="form-group col-md-2 align-self-center">
                                                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container End -->
    </section>

    <!--===================================
    =            Client Slider            =
    ====================================-->


    <!--===========================================
    =            Popular deals section            =
    ============================================-->





    <!--==========================================
    =            All Category Section            =
    ===========================================-->

    <section class=" section">
        <!-- Container Start -->
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section title -->

                    <div class="row">
                        <!-- Category list -->
                        <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">
                            <div class="category-block">
                                <div class="header">
                                    <i class="fa fa-laptop icon-bg-1"></i>
                                    <h4>Java</h4>
                                </div>
                                <ul class="category-list" >
                                    <li><a href="category.html">Tổng tin <span>93</span></a></li>
                                    <li><a href="category.html">Người  cần tuyển <span>233</span></a></li>

                                </ul>
                            </div>
                        </div> <!-- /Category List -->
                        <!-- Category list -->
                        <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">
                            <div class="category-block">
                                <div class="header">
                                    <i class="fa fa-apple icon-bg-2"></i>
                                    <h4>IOS</h4>
                                </div>
                                <ul class="category-list" >
                                    <li><a href="category.html">Tổng tin <span>93</span></a></li>
                                    <li><a href="category.html">Người  cần tuyển <span>233</span></a></li>

                                </ul>
                            </div>
                        </div> <!-- /Category List -->
                        <!-- Category list -->
                        <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">
                            <div class="category-block">
                                <div class="header">
                                    <i class="fa fa-home icon-bg-3"></i>
                                    <h4>C#</h4>
                                </div>
                                <ul class="category-list" >
                                    <li><a href="category.html">Tổng tin <span>93</span></a></li>
                                    <li><a href="category.html">Người  cần tuyển <span>233</span></a></li>

                                </ul>
                            </div>
                        </div> <!-- /Category List -->
                        <!-- Category list -->
                        <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">
                            <div class="category-block">
                                <div class="header">
                                    <i class="fa fa-shopping-basket icon-bg-4"></i>
                                    <h4>Python</h4>
                                </div>
                                <ul class="category-list" >
                                    <li><a href="category.html">Tổng tin <span>93</span></a></li>
                                    <li><a href="category.html">Người  cần tuyển <span>233</span></a></li>

                                </ul>
                            </div>
                        </div> <!-- /Category List -->
                        <!-- Category list -->
                        <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">
                            <div class="category-block">
                                <div class="header">
                                    <i class="fa fa-briefcase icon-bg-5"></i>
                                    <h4>Android Studio</h4>
                                </div>
                                <ul class="category-list" >
                                    <li><a href="category.html">Tổng tin <span>93</span></a></li>
                                    <li><a href="category.html">Người  cần tuyển <span>233</span></a></li>

                                </ul>
                            </div>
                        </div> <!-- /Category List -->
                        <!-- Category list -->
                        <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">
                            <div class="category-block">
                                <div class="header">
                                    <i class="fa fa-car icon-bg-6"></i>
                                    <h4>PHP</h4>
                                </div>
                                <ul class="category-list" >
                                    <li><a href="category.html">Tổng tin <span>93</span></a></li>
                                    <li><a href="category.html">Người  cần tuyển <span>233</span></a></li>

                                </ul>
                            </div>
                        </div> <!-- /Category List -->
                        <!-- Category list -->
                        <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">
                            <div class="category-block">
                                <div class="header">
                                    <i class="fa fa-paw icon-bg-7"></i>
                                    <h4>C++</h4>
                                </div>
                                <ul class="category-list" >
                                    <li><a href="category.html">Tổng tin <span>93</span></a></li>
                                    <li><a href="category.html">Người  cần tuyển <span>233</span></a></li>

                                </ul>
                            </div>
                        </div> <!-- /Category List -->
                        <!-- Category list -->
                        <div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6">
                            <div class="category-block">

                                <div class="header">
                                    <i class="fa fa-laptop icon-bg-8"></i>
                                    <h4>Note Js</h4>
                                </div>
                                <ul class="category-list" >
                                    <li><a href="category.html">Tổng tin <span>93</span></a></li>
                                    <li><a href="category.html">Người  cần tuyển <span>233</span></a></li>

                                </ul>
                            </div>
                        </div> <!-- /Category List -->


                    </div>
                </div>
            </div>
        </div>
        <!-- Container End -->
    </section>

    <!--====================================
    =            Call to Action
@endsection
