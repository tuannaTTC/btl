@extends('User.layout.layout')
@section('content')
    <section class="section bg-gray">
        <!-- Container Start -->
        <div class="container">
            <div class="row">
                <!-- Left sidebar -->
                <div class="col-md-8">
                    <div class="product-details">
                        <h1 class="product-title">Hp Dual Core 2gb Ram-Slim Laptop Available In Very Low Price</h1>
                        <div class="product-meta">
                            <ul class="list-inline">
                                <li class="list-inline-item"><i class="fa fa-user-o"></i> By <a href="">Andrew</a></li>
                                <li class="list-inline-item"><i class="fa fa-folder-open-o"></i> Category<a href="">Electronics</a></li>
                                <li class="list-inline-item"><i class="fa fa-location-arrow"></i> Location<a href="">Dhaka Bangladesh</a></li>
                            </ul>
                        </div>

                        <!-- product slider -->

                        <!-- product slider -->

                        <div class="content mt-5 pt-5">

                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <h3 class="tab-title">Product Description</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia laudantium beatae quod perspiciatis, neque
                                        dolores eos rerum, ipsa iste cum culpa numquam amet provident eveniet pariatur, sunt repellendus quas
                                        voluptate dolor cumque autem molestias. Ab quod quaerat molestias culpa eius, perferendis facere vitae commodi
                                        maxime qui numquam ex voluptatem voluptate, fuga sequi, quasi! Accusantium eligendi vitae unde iure officia
                                        amet molestiae velit assumenda, quidem beatae explicabo dolore laboriosam mollitia quod eos, eaque voluptas
                                        enim fuga laborum, error provident labore nesciunt ad. Libero reiciendis necessitatibus voluptates ab
                                        excepturi rem non, nostrum aut aperiam? Itaque, aut. Quas nulla perferendis neque eveniet ullam?</p>
                                    <p></p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam sed, officia reiciendis necessitatibus
                                        obcaecati eum, quaerat unde illo suscipit placeat nihil voluptatibus ipsa omnis repudiandae, excepturi! Id
                                        aperiam eius perferendis cupiditate exercitationem, mollitia numquam fuga, inventore quam eaque cumque fugiat,
                                        neque repudiandae dolore qui itaque iste asperiores ullam ut eum illum aliquam dignissimos similique! Aperiam
                                        aut temporibus optio nulla numquam molestias eum officia maiores aliquid laborum et officiis pariatur,
                                        delectus sapiente molestiae sit accusantium a libero, eligendi vero eius laboriosam minus. Nemo quibusdam
                                        nesciunt doloribus repellendus expedita necessitatibus velit vero?</p>

                                </div>
                                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                    <h3 class="tab-title">Product Specifications</h3>
                                    <table class="table table-bordered product-table">
                                        <tbody>
                                        <tr>
                                            <td>Seller Price</td>
                                            <td>$450</td>
                                        </tr>
                                        <tr>
                                            <td>Added</td>
                                            <td>26th December</td>
                                        </tr>
                                        <tr>
                                            <td>State</td>
                                            <td>Dhaka</td>
                                        </tr>
                                        <tr>
                                            <td>Brand</td>
                                            <td>Apple</td>
                                        </tr>
                                        <tr>
                                            <td>Condition</td>
                                            <td>Used</td>
                                        </tr>
                                        <tr>
                                            <td>Model</td>
                                            <td>2017</td>
                                        </tr>
                                        <tr>
                                            <td>State</td>
                                            <td>Dhaka</td>
                                        </tr>
                                        <tr>
                                            <td>Battery Life</td>
                                            <td>23</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                    <h3 class="tab-title">Product Review</h3>
                                    <div class="product-review">
                                        <div class="media">
                                            <!-- Avater -->
                                            <img src="images/user/user-thumb.jpg" alt="avater">
                                            <div class="media-body">
                                                <!-- Ratings -->
                                                <div class="ratings">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item">
                                                            <i class="fa fa-star"></i>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <i class="fa fa-star"></i>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <i class="fa fa-star"></i>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <i class="fa fa-star"></i>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <i class="fa fa-star"></i>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="name">
                                                    <h5>Jessica Brown</h5>
                                                </div>
                                                <div class="date">
                                                    <p>Mar 20, 2018</p>
                                                </div>
                                                <div class="review-comment">
                                                    <p>
                                                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremqe laudant tota rem ape
                                                        riamipsa eaque.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-submission">
                                            <h3 class="tab-title">Submit your review</h3>
                                            <!-- Rate -->
                                            <div class="rate">
                                                <div class="starrr"></div>
                                            </div>
                                            <div class="review-submit">
                                                <form action="#" class="row">
                                                    <div class="col-lg-6">
                                                        <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                                                    </div>
                                                    <div class="col-12">
                                                        <textarea name="review" id="review" rows="10" class="form-control" placeholder="Message"></textarea>
                                                    </div>
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn-main">Sumbit</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="sidebar">

                        <!-- User Profile widget -->
                        <div class="widget user text-center">
                            <img class="rounded-circle img-fluid mb-5 px-5" src="images/user/user-thumb.jpg" alt="">
                            <h4><a href="">Jonathon Andrew</a></h4>
                            <p class="member-time">Member Since Jun 27, 2017</p>
                        </div>
                        <div class="widget category">
                            <!-- Widget Header -->
                            <h5 class="widget-header">Categories</h5>
                            <ul class="category-list">
                                <li><a href="">Appearel <span class="float-right">(2)</span></a></li>
                                <li><a href="">Accesories <span class="float-right">(5)</span></a></li>
                                <li><a href="">Business<span class="float-right">(7)</span></a></li>
                                <li><a href="">Entertaiment<span class="float-right">(3)</span></a></li>
                                <li><a href="">Education<span class="float-right">(9)</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- Container End -->
    </section>
@endsection
