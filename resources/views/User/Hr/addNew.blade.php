@extends('User.layout.layout')
@section('content')
    <section class="ad-post bg-gray py-5">
        <div class="container">
            <form action={{route('hr.addNewPost')}} method="POST">
                @csrf
                <!-- Post Your ad start -->
                <fieldset class="border border-gary p-4 mb-5">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3>Đăng tin tuyển dụng</h3>
                        </div>
                        <div class="col-lg-6">
                            <h6 class="font-weight-bold pt-4 pb-1">Tiêu đề</h6>
                            <input type="text" class="border w-100 p-2 bg-white text-capitalize" placeholder="Tiêu đề" name="title">
                            <h6 class="font-weight-bold pt-4 pb-1">Yêu cầu giới tính</h6>
                            <div class="row px-3">
                                <div class="col-lg-3 mr-lg-4 my-2 rounded bg-white">
                                    <input type="radio" name="sex" value="nam" id="nam">
                                    <label for="nam" class="py-2">Nam</label>
                                </div>
                                <div class="col-lg-3 mr-lg-4 my-2 rounded bg-white ">
                                    <input type="radio" name="sex" value="nu" id="nu">
                                    <label for="nu" class="py-2">Nữ</label>
                                </div>
                                <div class="col-lg-3 mr-lg-4 my-2 rounded bg-white ">
                                    <input type="radio" name="sex" value="all" id="all">
                                    <label for="all" class="py-2">All</label>
                                </div>
                            </div>
                            <h6 class="font-weight-bold pt-4 pb-1">Nội dung tuyển dụng</h6>
                            <textarea name="contentNews" id="" class="border p-3 w-100" rows="7" placeholder="Nội dung tuyển dụng"></textarea>
                        </div>
                        <div class="col-lg-6">
                            <h6 class="font-weight-bold pt-4 pb-1">Công việc cần tuyển</h6>
                            <select name="jobId" id="inputGroupSelect" class="w-100">
                                <option value="1">Công việc cần tuyển</option>
                                <option value="2">Laptops</option>
                                <option value="3">iphone</option>
                                <option value="4">microsoft</option>
                                <option value="5">monitors</option>
                                <option value="6">11inch Macbook Air</option>
                                <option value="7">Study Table Combo</option>
                                <option value="8">11inch Macbook Air</option>
                                <option value="9">Study Table Combo</option>
                                <option value="10">11inch Macbook Air</option>
                            </select>
                            <div class="price">
                                <h6 class="font-weight-bold pt-4 pb-1">Mức lương - phụ cấp (VNĐ):</h6>
                                <div class="row px-3">
                                    <div class="col-lg-4 mr-lg-4 rounded bg-white my-2 ">
                                        <input type="text" name="salary" class="border-0 py-2 w-100 price" placeholder="Mức lương"
                                               id="price">
                                    </div>
                                </div>
                            </div>
                            <h6 class="font-weight-bold pt-4 pb-1">Thời gian</h6>
                            <div class="row px-3">
                                <div class="col-lg-5 mr-lg-4 my-2 rounded bg-white">
                                    <input type="radio" name="times" value="fulltime" id="fulltime">
                                    <label for="fulltime" class="py-2">Toàn thời gian</label>
                                </div>
                                <div class="col-lg-5 mr-lg-4 my-2 rounded bg-white ">
                                    <input type="radio" name="times" value="partime" id="partime">
                                    <label for="partime" class="py-2">Bán thời gian</label>
                                </div>
                            </div>
                            <h6 class="font-weight-bold pt-4 pb-1">Yêu cầu kinh nghiệm</h6>
                            <input name="experience" type="text" class="border w-100 p-2 bg-white text-capitalize" placeholder="Yêu cầu kinh nghiệm">
                            <h6 class="font-weight-bold pt-4 pb-1">Ngày hết hạn nộp CV</h6>
                            <div class="col-sm-5">
                                <input type="date" class="form-control" id="inputEmail3" placeholder="Ngày sinh" name="endDate">
                            </div>
                        </div>
                    </div>
                </fieldset>
                <!-- Post Your ad end -->

                <!-- seller-information start -->

                <!-- submit button -->

                <button type="submit" class="btn btn-primary d-block mt-2">Đăng tin tuyển dụng</button>
            </form>
        </div>
    </section>
@endsection
