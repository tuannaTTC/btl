<?php

namespace App\Http\Controllers;
use App\ViTri;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    //
    private $user;
    private $viTri;

    public function __construct(User $user,ViTri $viTri){
        $this->user = $user;
        $this->viTri = $viTri;
    }


    //Login
    public function loginGet(){
        return view('login.login');

    }

    public function loginPost(Request $request){
        if(Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,

        ])){
           // return redirect()->route('newHome');
            return view('newHome');
        }
        else{
            return redirect()->route('login.index')->with('thongbao','Đăng nhập thất bại!');
        }
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('home.index');
    }

    //Std
    public function registerStudentGet(){
        return view('login.registerStd');
    }

    public function registerStudentPost(Request $request){
        if($request->password != $request->repeatPassword){
            return redirect()->route('login.registerStdGet')->with('thongbao',"Mật khẩu chưa giống nhau!");
        }
        elseif($this->user->where('email','=',$request->email)->exists()){
            return redirect()->route('login.registerStdGet')->with('thongbao','Email đã tồn tại!');
        }
        elseif (!$request->hasFile('linkCv')){
            return redirect()->route('login.registerStdGet')->with('thongbao','Chưa tải cv lên');
        }
        else{
            $request->file('linkCv')->move(
                'CvStudent',
                $request->email . '.docx'
            );
            $this->user->create([
                'name' => $request->name,
                'password' => bcrypt($request->password),
                'email' => $request->email,
                'phoneNum' => $request->phoneNum,
                'birthday' => $request->birthday,
                'roleId' => config('constant.STUDENT'),
                'sex' => $request->sex,
                'address' => $request->address,
                'activeFlag' => 1,
                'linkCv' => $request->email
            ]);
            Auth::attempt([
                'email' => $request->email,
                'password' => $request->password
            ]);
            return redirect()->route('home.index');
        }

    }

    //Hr
    public function registerHrGet(){
        $viTri = $this->viTri->get();
        return view('login.registerHr',compact('viTri'));
    }

    public function registerHrPost(Request $request){
        if($request->password != $request->repeatPassword){
            return redirect()->route('login.registerHrGet')->with('thongbao',"Mật khẩu chưa giống nhau!");
        }
        elseif($this->user->where(['email','=',$request->email])->exists()){
            return redirect()->route('login.registerHrGet')->with('thongbao','Email đã tồn tại!');
        }
        elseif (!$request->hasFile('linkLogo')){
            return redirect()->route('login.registerHrGet')->with('thongbao','Chưa tải cv lên');
        }
        else{
            $request->file('linkLogo')->move(
                'LogoCty',
                $request->email . '.jpg'
            );
            $this->user->create([
                'name' => $request->name,
                'password' => bcrypt($request->password),
                'email' => $request->email,
                'phoneNum' => $request->phoneNum,
                'birthday' => $request->birthday,
                'roleId' => config('constant.HR'),
                'sex' => $request->sex,
                'address' => $request->address,
                'activeFlag' => 1,
                'linkLogo' => $request->email,
                'companyAddress' => $request->companyAddr,
                'companyName' => $request->companyName,
                'positionId' => $request->positionId
            ]);
            Auth::attempt([
                'email' => $request->email,
                'password' => $request->password
            ]);
            return redirect()->route('home.index');
        }
    }


}
