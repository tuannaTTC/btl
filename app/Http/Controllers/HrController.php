<?php

namespace App\Http\Controllers;

use App\TinTuyenDung;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HrController extends Controller
{
    private $news;

    //
    public function __construct(TinTuyenDung $tinTuyenDung)
    {
        $this->news = $tinTuyenDung;
    }

    public function addNewGet()
    {
        return view('User.Hr.addNew');
    }

    public function addNewPost(Request $request)
    {

        $this->news->create([
            'userId' => Auth::user()->id,
            'title' => $request->title,
            'sex' => $request->sex,
            'content' => $request->contentNews,
            'jobId' => $request->jobId,
            'salary' => $request->salary,
            'time' => $request->times,
            'experience' => $request->experience,
            'position' => Auth::user()->companyAddress,
            'enddate' => $request->endDate,
            'activeFlag' => 1
        ]);

        return view('User.Hr.addNew');
    }

}
