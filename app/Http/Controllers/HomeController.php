<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function index(){
        return view('newHome');
    }

    public function showJobGet(){
        return view('User.Home.showJob');
    }

    public function jobDetailGet(){
        return view('User.home.jobDetail');
    }

}
